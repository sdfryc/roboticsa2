%% This interface inherits the Singleton
%% The only instance of that class is obtainable by calling 
%% Vision.instance()
%%
%% Determines the pose of coloured, rectanguar objects against white background
%%
%% Author: Simon Fryc

%% Class definition
classdef Vision < Singleton
    
    properties % Public Access      
    end
    
    methods(Access=private)
        %% Constructor
        function self = Vision()
        end
    end
    
    methods
        %% Vision algorithm
        function imageSpacePose = ImageSpacePose(self,image)
            
            % Import image
            im = imread('testImg.png');
            imshow(im);
            im = im2double(im);
            
            % Get rows, columns and colour channels
            [r, c, p] = size(im);
            
            % Get RBG channels, squeezing singletons          
            imR = squeeze(im(:, :, 1));
            imG = squeeze(im(:, :, 2));
            imB = squeeze(im(:, :, 3));
            
            % Convert each channel to binary
            imBinaryR = im2bw(imR,graythresh(imR));
            imBinaryG = im2bw(imG,graythresh(imG));
            imBinaryB = im2bw(imB,graythresh(imB));
            imBinary = imcomplement(imBinaryR&imBinaryG&imBinaryB);
            imshow(imBinary)
            
            % Perform disk, fill and border cleanup           
            se = strel('disk',7);
            imClean = imopen(imBinary,se);
            imClean = imfill(imClean,'holes');
            imClean = imclearborder(imClean);
            imshow(imClean)
            
            % Label regions
            [labels,numLabels] = bwlabel(imClean);
            disp(['Number of objects detected: ' num2str(numLabels)]);
            
            % Create some RBG variables for colour averaging
            rLabel = zeros(r,c);
            gLabel = zeros(r,c);
            bLabel = zeros(r,c);
            rTally = 0;
            gTally = 0;
            bTally = 0;
            
            % Average the colours inside each region
            for i = 1:numLabels
                rLabel(labels==i) = median(imR(labels==i));
                gLabel(labels==i) = median(imG(labels==i));
                bLabel(labels==i) = median(imB(labels==i));
                rTally = rTally + median(imR(labels==i));
                gTally = gTally + median(imG(labels==i));
                bTally = bTally + median(imB(labels==i));
            end
            
            % Determine average colours for each region
            rAvg = rTally / numLabels;
            gAvg = gTally / numLabels;
            bAvg = bTally / numLabels;
            imLabel = cat(3,rLabel,gLabel,bLabel);
            imshow(imLabel)
            R = rAvg;
            G = gAvg;
            B = bAvg;
            
            % Reload origional image
            image = imread('testImg.png');
            image = im2double(image);
            
            % Get colour channel information
            channelR = image(:, :, 1);
            channelG = image(:, :, 2);
            channelB = image(:, :, 3);
            
            % calculate overall distance from the given RGB color
            dR = channelR - R;
            dG = channelG - G;
            dB = channelB - B;
            rSqr = dR.*dR;
            gSqr = dG.*dG;
            bSqr = dB.*dB;
            d = rSqr + gSqr + bSqr;
            
            % create a mask by thresholding the differences
            mask = d < 0.016;
            imshow(mask)
            
            % Perform disk, fill and border cleanup                
            se = strel('disk',4);
            imClean = imopen(mask,se);
            imClean = imfill(imClean,'holes');
            imClean = imclearborder(imClean);
            imshow(imClean);
            imshow(imClean)
            
            % Determin the region properties
            stats = regionprops('table',imClean,...
                'Centroid','Orientation')
            
            % Return the region properties            
            imageSpacePose = table2array(stats);
        end
    end
    
    %% Internal method for singleton implementation
    methods(Static)
        function self = instance()
            persistent uniqueInstance
            if isempty(uniqueInstance)
                self = Vision();
                uniqueInstance = self;
            else
                self = uniqueInstance;
            end
        end
    end
end