%% Script for testing the interface to the arduino

close all
clear
clc

interface = ArduinoInterface.instance();

interface.Restart();

input('Press ENTER to close\n');
interface.Close();