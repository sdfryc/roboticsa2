%% This interface inherits the Singleton
%% The only instance of that class is obtainable by calling 
%% ArduinoInterface.instance()
%%
%% Autor: Moritz Oertel

classdef ArduinoInterface < Singleton
    properties % Public Access
        %% Defines
        % Serial connection
        BAUD_RATE = 9600;
        % Light curtain
        STATUS_STARTED = 0;
        STATUS_CLEAR = 1;
        STATUS_DETECTED = 2;
        
        %% Variables
        serialObject;
        lightCurtainStatus;
    end

    %% !Private! Constructor 
    methods(Access = private)
        function self = ArduinoInterface()
            % Get the serial port dynamically
            serialPort = seriallist;
            % Init connection
            self.serialObject = serial(serialPort, 'BaudRate', self.BAUD_RATE, ...
                'timeout', inf, ...
                'Terminator', 'CR/LF');
            self.serialObject.BytesAvailableFcnMode = 'terminator';
            self.serialObject.BytesAvailableFcn = @self.SerialCallbackFct;
            
            fopen(self.serialObject);
            
            self.lightCurtainStatus = self.STATUS_STARTED;
            
            disp('ArduinoInterface UP');
        end
    end
    %% Basic methods
    methods % Public Access
        function Close(self)
            fclose(self.serialObject);
        end
        
        function Restart(self)
            fclose(self.serialObject);
            fopen(self.serialObject);
        end
        
        function lightCurtainStatus = GetLightCurtainStatus(self)
            lightCurtainStatus = self.lightCurtainStatus;
        end
    end
    
    %% Private methods
    methods (Access=private)
        function SerialCallbackFct(self, serialPortObject, event)
            % timeStamp = event.Data;
            readData = fgetl(self.serialObject);
            % readData
            if strcmp(readData,'started')
                self.lightCurtainStatus = self.STATUS_STARTED;
                disp("Received >started< status code");
            elseif strcmp(readData,'detected')
                self.lightCurtainStatus = self.STATUS_DETECTED;
                disp("Received >detected< status code");    
            elseif strcmp(readData,'clear')
                self.lightCurtainStatus = self.STATUS_CLEAR;
                disp("Received >clear< status code");    
            end
        end
    end
    
    %% Deconstructor
    methods
        function delete(self)
            fclose(self.serialObjec);
        end
    end
    %% Internal method for singleton implementation
    methods(Static)
      function self = instance()
         persistent uniqueInstance
         if isempty(uniqueInstance)
            self = ArduinoInterface();
            uniqueInstance = self;
         else
            self = uniqueInstance;
         end
      end
   end
end
