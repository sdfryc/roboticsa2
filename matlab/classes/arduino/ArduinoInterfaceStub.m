%% This interface inherits the Singleton
%% The only instance of that class is obtainable by calling 
%% ArduinoInterfaceStub.instance()
%%
%% Autor: Moritz Oertel

classdef ArduinoInterfaceStub < Singleton
    properties % Public Access
        %% Defines
        % Light curtain
        STATUS_STARTED = "STATUS_STARTED";
        STATUS_CLEAR = "STATUS_CLEAR";
        STATUS_DETECTED = "STATUS_DETECTED";
        
        %% Variables
        lightCurtainStatus;
    end

    %% !Private! Constructor 
    methods(Access = private)
        function self = ArduinoInterfaceStub()
            self.lightCurtainStatus = self.STATUS_STARTED;
            disp('ArduinoInterfaceStub UP');
        end
    end
    %% Basic methods
    methods % Public Access
        function Close(self)
        end
        
        function Restart(self)
            self.lightCurtainStatus = self.STATUS_STARTED;
        end
        
        function lightCurtainStatus = GetLightCurtainStatus(self)
            lightCurtainStatus = self.lightCurtainStatus;
        end
        
        function SetLightCurtainStatus(self, status)
            self.lightCurtainStatus = status;
        end
    end
    
    %% Private methods
    methods (Access=private)
    end
    
    %% Internal method for singleton implementation
    methods(Static)
      function self = instance()
         persistent uniqueInstance
         if isempty(uniqueInstance)
            self = ArduinoInterfaceStub();
            uniqueInstance = self;
         else
            self = uniqueInstance;
         end
      end
   end
end
