% SERIAL_PORT = '/dev/ttyACM1';

function serial_event
    global s
    SERIAL_PORT = seriallist
    BAUD_RATE = 9600;

    s = serial(SERIAL_PORT, 'BaudRate', BAUD_RATE, ...
        'timeout', inf, ...
        'Terminator', 'CR/LF');

    s.BytesAvailableFcnMode = 'terminator';
    s.BytesAvailableFcn = @serial_callback_fct;


    fopen(s);

    pause(0.5)
    input('Press enter to exit\n')
    fclose(s);
    delete(s)
    clear s
end

function serial_callback_fct(serialPortObject, event)
    global s
    % serialPortObject;
    % event;
    
    timeStamp = event.Data;
    readData = fgetl(s);
    readData
    if strcmp(readData,'started')
        disp("Received starting status code");
    end
    
    
    %'started'
    %'detected'
    %'clear'
end