clear
clc
close all

interface = DobotRosInterface.instance();

%% State Test - Passed?
interface.GetState()

%% Pump Test - Passed?
interface.SetPumpOff()
interface.SetPumpOn()

%% Joint Test - Passed?
delta = -45*pi/180;
interface.IncrementJointByDelta(0, delta);

%% Cart Test - Passed?
