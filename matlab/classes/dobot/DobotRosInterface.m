%% This interface inherits the Singleton
%% The only instance of that class is obtainable by calling 
%% DobotRosInterface.instance()
%%
%% Start ROS remotly through SSL with following commands:
%% >>ssh student@192.168.0.50
%% >>roscore
%% >>roslaunch dobot_magician dobot_magician.launch
%%
%% Autor: Moritz Oertel

%% Notes: Tooltip to Camera translation vector
%% 52mm in x; 15mm in y; 50mm in z  

classdef DobotRosInterface < Singleton

    properties % Public Access
        
        
        %% Set IP here
        IP_DOBOT_ROS_SERVER = '192.168.0.50';
        
        %% Defines
        STATE_MSG = '/dobot_magician/state';
        PUMP_MSG = '/dobot_magician/pump';
        JOINT_MSG = '/dobot_magician/joint_angs';
        CART_MSG = '/dobot_magician/cart_pos';
        
        TAKE_IMAGE_POSITION_ANGLES = [0.0 -0.0534 -0.2500 0.0];

        %% Subscribers
        % state
        stateSub;
        
        %% Clients
        % cartesian
        cartClient;
        cartMsg;
        % joint
        jointClient;
        jointMsg;
        % pump
        pumpClient;
        pumpMsg;        
    end
   
    methods(Access=private)
        % Constructor has to be private 
        function self = DobotRosInterface()
            
            % Check messages
            if ~find(cell2mat(strfind(rosmsg('list'),'dobot')),1)
                error("ROS DoBot messages are not loaded")
            end

            % Setup connection to ROS server
            rosshutdown
            rosinit(self.IP_DOBOT_ROS_SERVER);

            % Subscribe to state and create message objects once
            self.stateSub = rossubscriber(self.STATE_MSG);
            
            self.cartClient = rossvcclient(self.CART_MSG);
            self.cartMsg = rosmessage(self.cartClient);
            
            self.jointClient = rossvcclient(self.JOINT_MSG);
            self.jointMsg = rosmessage(self.jointClient);
            
            self.pumpClient = rossvcclient(self.PUMP_MSG);
            self.pumpMsg = rosmessage(self.pumpClient); 
            % or rosmessage(self.pumpClient); 
            
            % init
            self.SetPumpOff();
            % self.SetCartPosition([0 0 0 0]);
        end
    end
   
    %% Basic methods for the interface (on ros message level)
    methods % Public Access
        function SetPumpOn(self)
            %self.Log('SetPumpOn');
            self.pumpMsg.Pump = true;
            try
                self.pumpClient.call(self.pumpMsg);
            catch
                %pass
            end
        end
        
        function SetPumpOff(self)
            %self.Log('SetPumpOff');
            self.pumpMsg.Pump = false;
            try
                self.pumpClient.call(self.pumpMsg);
            catch
                
            end
        end
        
        % angles: [base, foreArm, rearArm, endEffectorServo] in rad
        function SetJointAngles(self, angles)
            %self.Log('SetJointAngles');
            self.jointMsg.JointAngles = angles;
            self.jointClient.call(self.jointMsg);
        end

        function SetCartPosition(self, position)
            %self.Log('SetCartPosition');
            self.cartMsg.Pos.X = position(1);
            self.cartMsg.Pos.Y = position(2);
            self.cartMsg.Pos.Z = position(3);
            self.cartClient.call(self.cartMsg);
        end

        % subscriber to state
        function state = GetState(self)
            
            %    MessageType: 'dobot_magician/State'
            %         Header: [1×1 Header]
            %            Pos: [1×1 Point]
            %           Pump: 0
            %         Status: 0
            %    JointAngles: [4×1 double]
            
            %ROS Point message with properties:

            %MessageType: 'geometry_msgs/Point'
            %          X: 0.2606
            %          Y: -1.2039e-04
            %          Z: -0.0084
            
            %self.Log('GetState');
            receive(self.stateSub, 2);
            state = self.stateSub.LatestMessage;
        end
    end
    
    %% Advanced methods - use the above methods here
    methods % Public Access
        function jointAngles = GetJointAngles(self)
            jointAngles = self.GetState().JointAngles;
        end
        
        function IncrementJointByDelta(self, jointIndex, delta)
            angles = self.GetJointAngles();
            angles(jointIndex) = angles(jointIndex) + delta;
            self.SetJointAngles(angles);
        end
        
        function position = GetTooltipPosition(self)
            state = self.GetState();
            x = state.Pos.X;
            y = state.Pos.Y;
            z = state.Pos.Z;
            position = [x y z];
        end
        
        function IncrementPositionByDelta(self, delta)
            position = self.GetTooltipPosition(); 
            newPos = position + delta;
            self.SetCartPosition(newPos);
        end
        
        function GoToCameraHome(self)
            self.SetJointAngles(self.TAKE_IMAGE_POSITION_ANGLES);
        end
    end
   
    %% Internal method for singleton implementation
    methods(Static)
      function self = instance()
         persistent uniqueInstance
         if isempty(uniqueInstance)
            self = DobotRosInterface();
            uniqueInstance = self;
         else
            self = uniqueInstance;
         end
      end
   end
end
