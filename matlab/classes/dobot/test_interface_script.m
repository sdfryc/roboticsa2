clear
clc
close all

%% Defines
STATE_MSG = '/dobot_magician/state';
PUMP_MSG = '/dobot_magician/pump';
JOINT_MSG = '/dobot_magician/joint_angs';
CART_MSG = '/dobot_magician/cart_pos';
IP_DOBOT_ROS_SERVER = '192.168.0.50';

%% Init Matlab - ROS connection
rosshutdown
rosinit(IP_DOBOT_ROS_SERVER);

%% State Test - Passed
stateSub = rossubscriber(STATE_MSG);
receive(stateSub, 2);
state = stateSub.LatestMessage;

%% Pump Test - Passed
pumpClient = rossvcclient(PUMP_MSG);
pumpMsg = rosmessage(pumpClient);
pumpMsg.Pump = false;
pumpClient.call(pumpMsg);

%% Joint Test - Passed 
jointClient = rossvcclient(JOINT_MSG);
jointMsg = rosmessage(jointClient);
jointMsg.JointAngles = state.JointAngles;
jointNumber = 4;
stepSize = -90*pi/180;
jointMsg.JointAngles(jointNumber) = jointMsg.JointAngles(jointNumber) + stepSize;
jointClient.call(jointMsg);

%% Cart Test
%cartClient = rossvcclient(CART_MSG);
%cartMsg = rosmessage(cartClient);
%cartMsg.Pos.X = x;
%cartMsg.Pos.Y = y;
%cartMsg.Pos.Z = z;
%cartClient.call(cartMsg);