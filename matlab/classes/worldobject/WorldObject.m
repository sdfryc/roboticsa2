%% Loads an object into the workspace
%% Author: Simon Fryc

%% Class definition
classdef WorldObject < handle
    
    properties % Public Access
        %% Variables
        name;
        base;
        faces;
        faceNormals;
        verts;
        vertexCount;
        vertexColours;
        mesh;
    end
    
 %% Basic methods
    methods % Public Access
        % Constructor
        function self = WorldObject(partFile,name,base)
            
            % Import the ply file for the object
            self.name = name;
            [f,v,data] = plyread(partFile,'tri');
            
            % Obtain the faces, verticies and the number of verticies
            self.faces = f;
            self.verts = v;
            self.vertexCount = size(v,1);
            
            self.faceNormals = zeros(size(f,1),3);
            for faceIndex = 1:size(f,1)
                v1 = v(f(faceIndex,1)',:);
                v2 = v(f(faceIndex,2)',:);
                v3 = v(f(faceIndex,3)',:);
                try
                    self.faceNormals(faceIndex,:) = unit(cross(v2-v1,v3-v1));
                catch   
                end
            end
            
            % Define the objects base
            self.base = base;
            
            % Scale the colours to be 0-to-1 (originally 0-to-255), apply to the object
            self.vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
            
            % Then plot the trisurf of the object
            self.mesh = trisurf(f,self.verts(:,1),self.verts(:,2), self.verts(:,3) ...
                ,'FaceVertexCData',self.vertexColours,'EdgeColor','interp','EdgeAlpha', 0,'EdgeLighting','flat');
            
            % Define the points of the objects base
            updatedPoints = [self.base * [self.verts,ones(self.vertexCount,1)]']';
            
            % Now update the vertices
            self.mesh.Vertices = updatedPoints(:,1:3);

            % Log object creation
            LoggerInterface.instance().Log(['WorldObject','World object created with name ', name]);
            
        end

        %% Return face,vert & face normals
        function [vertex,face,faceNormals] = GetModelData(self)
            vertex = self.mesh.Vertices;
            face = self.faces;
            faceNormals = self.faceNormals;
        end
             
    end
end