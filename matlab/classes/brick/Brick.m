%% Creates and plots a brick object
%% Author: Simon Fryc

%% Class definition
classdef Brick < handle
    
    properties (Constant) % Public Access
        brickFile = 'Brick.ply';
        dimensions = [0.042, 0.025, 0.019];
    end
    
    properties % Public Access
        %% Variables
        name;
        base;
        faces;
        faceNormals;
        verts;
        vertexCount;
        vertexColours;
        mesh;
    end
    
    methods
        % Constructor
        function self = Brick(name,base)
            
            % Import the ply file for the brick
            self.name = name;
            [f,v,data] = plyread(self.brickFile,'tri');
            
            % Obtain the faces, verticies and face normals
            self.faces = f;
            self.verts = v;
            self.vertexCount = size(v,1);
            self.faceNormals = zeros(size(f,1),3);
            for faceIndex = 1:size(f,1)
                v1 = v(f(faceIndex,1)',:);
                v2 = v(f(faceIndex,2)',:);
                v3 = v(f(faceIndex,3)',:);
                self.faceNormals(faceIndex,:) = unit(cross(v2-v1,v3-v1));
            end
            
            % Define the bricks base
            self.base = base;
            
            % Scale the colours to be 0-to-1 (originally 0-to-255), apply to the brick
            self.vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
            
            % Then plot the trisurf of the brick
            self.mesh = trisurf(f,self.verts(:,1),self.verts(:,2), self.verts(:,3) ...
                ,'FaceVertexCData',self.vertexColours,'EdgeColor','interp','EdgeLighting','flat');
            
            % Define the points of the bricks base
            updatedPoints = [self.base * [self.verts,ones(self.vertexCount,1)]']';
            
            % Now update the vertices
            self.mesh.Vertices = updatedPoints(:,1:3);
            
            % Log brick
            LoggerInterface.instance().Log('Brick','Brick ojbect created');
        end
        
        %% Move a brick to a new base
        function [] = MoveBrick(self,newPosition)
            % Define the points of the bricks base
            newPosition(3,4) = newPosition(3,4) - self.dimensions(3);
            updatedPoints = [newPosition * [self.verts,ones(self.vertexCount,1)]']';
            
            % Now update the vertices
            self.mesh.Vertices = updatedPoints(:,1:3);
        end
        
        %% Return the base of the brick
        function base = GetBase(self)
            % Return the base of the brick
            base = self.base;
        end
        
        %% Return a pose for grasping the brick
        function pose = GetGraspPose(self)
            pose = self.base;
            pose(3,4) = pose(3,4) + self.dimensions(3);
        end
        
        %% Set the brick base
        function [] = UpdateBase(self)      
            % Define the points of the bricks base
            updatedPoints = [self.base * [self.verts,ones(self.vertexCount,1)]']';
            % Now update the vertices
            self.mesh.Vertices = updatedPoints(:,1:3);
        end
        
        %% Return face,vert & face normals
        function [vertex,face,faceNormals] = GetModelData(self)
            vertex = self.verts;
            face = self.faces;
            faceNormals = self.faceNormals;
        end
        
        %% Delete the object mesh
        function Delete(self)
            delete(self.mesh);
        end
    end
end