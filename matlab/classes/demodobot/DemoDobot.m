%% This interface inherits the Singleton
%% The only instance of that class is obtainable by calling 
%% DemoDobot.instance()
%%
%% Performs two demonstrations of the Dobot tasks
%%
%% Author: Simon Fryc

%% Class definition
classdef DemoDobot < Singleton
    
    methods(Access=private)
        % Constructor has to be private
        function self = DemoDobot()
        end
    end
    
    %% Basic methods
    methods % Public Access
        
        % Demonstrate the construction of a wall based on operator inputs
        function WallBuilding(~,numberOfBricks,centerOfBricks,wallOrigin)
            % Define viewpoint
            view(100,20);
            
            % Turn on the light curtain before any movements
            LightCurtain.instance().Show();
            
            % Home the Dobot
            SimulatedDobot.instance().Home();
            
            % Generate a random pile of bricks as per operator inputs
            pile = RandomBricks(numberOfBricks,centerOfBricks);
            objectList = pile.GetBricks();
            endPoints = pile.ConfigureWall(wallOrigin);
            
            % Pick and place bricks to build the wall
            SimulatedDobot.instance().PickAndPlace(objectList, endPoints);
            
            % Return the Dobot to home position
            SimulatedDobot.instance().Home();
            
            % Turn off light curtain
            LightCurtain.instance().Hide();
            
            % Delete the bricks
            pile.Delete();
        end
        
        % Demonstrate the robots reaction to a forced collision
        function ForcedCollision(~)
            
            % Define viewpoint
            view(100,20);
            
            % Define a series of waypoints
            wayPoint1 = [0.25  0.00 0.10];          
            wayPoint2 = [0.30 -0.25 0.20];
            wayPoint3 = [0.30  0.25 0.30];
            
            % Turn on the light curtain before any movements
            LightCurtain.instance().Show();
            
            % Home the Dobot
            SimulatedDobot.instance().Home();
             
            % Add two cylinder objects to the robot environment
            DobotEnvironment.instance().AddObject('cylinder.ply','cylinder1',eye(4)*transl(0.18, 0.15, 0.00));
            DobotEnvironment.instance().AddObject('cylinder.ply','cylinder2',eye(4)*transl(0.18, -0.15, 0.00));
            
            % Execute the movements between waypoints
            SimulatedDobot.instance().SetCartPosition(wayPoint1);
            SimulatedDobot.instance().SetCartPosition(wayPoint2);
            SimulatedDobot.instance().SetCartPosition(wayPoint3); 
            
            % Home the Dobot
            SimulatedDobot.instance().Home();
            
            % Delete the cylinder objects
            DobotEnvironment.instance().DeleteObject('cylinder1');
            DobotEnvironment.instance().DeleteObject('cylinder2');
            
            % Turn off the light curtain
            LightCurtain.instance().Hide();
        end
        
    end
    
    %% Internal method for singleton implementation
    methods(Static)
        function self = instance()
            persistent uniqueInstance
            if isempty(uniqueInstance)
                self = DemoDobot();
                uniqueInstance = self;
            else
                self = uniqueInstance;
            end
        end
    end
end

