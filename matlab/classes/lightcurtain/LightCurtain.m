%% Illistrates a light curtain in a fixed position
%% Author: Simon Fryc

%% Class definition
classdef LightCurtain < Singleton
    
    properties % Public Access
        
        %% Variables
        lines
        
        %% Constant
        CURTAIN_LINES = 20;
        LINE_WIDTH = 1;
        CURTAIN_LENGTH = 0.76;
        INCREMENT = 0.038;
        START_POS = -0.38;
    end
    
    methods(Access=private)
        %% Constructor
        function self = LightCurtain()
        end
    end
    
    methods
        %% Show model light curtain
        function Show(self)
            yPos = self.START_POS;
            for i = 1:1:(self.CURTAIN_LINES+1)
                linePoints = [0.38 yPos 0.0;
                              0.38 yPos 0.7];
                yPos = yPos + self.INCREMENT;
                self.lines{i} = plot3(linePoints(:,1),linePoints(:,2),linePoints(:,3),'r','LineWidth',self.LINE_WIDTH);
            end
        end
        
        %% Hide model off light curtain
        function [] = Hide(self)
            for i = 1:1:length(self.lines)
               delete(self.lines{i}); 
            end
        end
    end
    
    %% Internal method for singleton implementation
    methods(Static)
        function self = instance()
            persistent uniqueInstance
            if isempty(uniqueInstance)
                self = LightCurtain();
                uniqueInstance = self;
            else
                self = uniqueInstance;
            end
        end
    end
end