%% This interface inherits the Singleton
%% The only instance of that class is obtainable by calling 
%% LoggerInterface.instance()
%%
%% Autor: Moritz Oertel

classdef LoggerInterface < Singleton

    properties % Public Access
        %% Defines
        LOGGER_FILE_NAME = 'test_logger.log';
        % Chosse levels from [DEBUG,WARN,ERROR,NONE]
        LOGGER_WINDOW_LEVEL = log4matlab.NONE;
        LOGGER_LOG_LEVEL = log4matlab.DEBUG;
        
        %% Variables
        loggerObject;
    end
   
    %% On delete function implementation
    methods
        function delete(self)
            disp('LoggerInterface DOWN'); % never gets called?
        end
    end
    
    %% Private constructor
    methods(Access=private)
        % Constructor has to be private 
        function self = LoggerInterface()
            self.loggerObject = log4matlab(self.LOGGER_FILE_NAME);
            self.loggerObject.SetCommandWindowLevel(self.LOGGER_WINDOW_LEVEL);
            disp('LoggerInterface UP');
            disp(['Logfile path: ' pwd '/' self.LOGGER_FILE_NAME]);
        end
    end
    
    %% Basic methods
    methods % Public Access
        function Log(self, tag, msg)
            self.loggerObject.mlog = {self.LOGGER_LOG_LEVEL, tag, msg};
        end
    end
    
    %% Private methods
    methods (Access=private)
        
    end
    
    %% Internal method for singleton implementation
    methods(Static)
      function self = instance()
         persistent uniqueInstance
         if isempty(uniqueInstance)
            self = LoggerInterface();
            uniqueInstance = self;
         else
            self = uniqueInstance;
         end
      end
   end
end
