%% Assignment 2 - Robot Mason Simulation
function main()
%% Clear workspace variables
clc;
clear all;
close all;

%% Demonstration inputs
numberOfBricks = 3;
centerOfBricks = [0.22, -0.15, 3/4 * pi];
wallOrigin = [0.2 0.2 3/4*pi];

%% Perform demonstrations
db = SimulatedDobot.instance();
DemoDobot.instance().WallBuilding(numberOfBricks,centerOfBricks,wallOrigin);
DemoDobot.instance().ForcedCollision();

%% Alternatively, use the graphical user interface
Dobot_Controller
end
