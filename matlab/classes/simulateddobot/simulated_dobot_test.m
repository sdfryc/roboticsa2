%% SimulatedDobot Class Tests
clear all;
close all;
clc

workspaceDimensions = [-0.5 0.5 -0.5 0.5 -0.1 0.8];
WorkspaceInterface.instance().SetWorkspace(workspaceDimensions);

SimulatedDobot.instance();

%% Test Move Function
angles = deg2rad([0 45 45 -120 0]);
SimulatedDobot.instance().Move(angles)

%% Test GetJointAngles Function
SimulatedDobot.instance().GetJointAngles()

%% Test SetCartPosition Function
xPos = 0.268;
yPos = 0.000;
zPos = 0.057;
position = [xPos yPos zPos];
SimulatedDobot.instance().SetCartPosition(position);

%% Test GetTooltipPosition Function
SimulatedDobot.instance().GetTooltipPosition()

%% Test SetSingleJointAngle Function
SimulatedDobot.instance().SetSingleJointAngle(1,60);
SimulatedDobot.instance().SetSingleJointAngle(1, 0);
SimulatedDobot.instance().SetSingleJointAngle(3,30);
SimulatedDobot.instance().SetSingleJointAngle(1,-60);

%% Test IncrementJointByDelta Function
SimulatedDobot.instance().IncrementJointByDelta(1,20);
SimulatedDobot.instance().IncrementJointByDelta(1,20);
SimulatedDobot.instance().IncrementJointByDelta(1,20);

%% Test IncrementCartByDelta Function
deltaXYZ = [0.00, 0.00, 0.05];
SimulatedDobot.instance().IncrementCartByDelta(deltaXYZ)

%% Test Home Function
SimulatedDobot.instance().Home()
