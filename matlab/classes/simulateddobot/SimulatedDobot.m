%% This interface inherits the Singleton
%% The only instance of that class is obtainable by calling
%% SimulatedDobot.instance()
%%
%% Contains a workspace interface
%%
%% Author: Simon Fryc
classdef SimulatedDobot < Singleton
    
    properties % Public Access
        
        %% Defines
        NAME = 'dobot';
        BASE_POSE = eye(4);
        QLIMS = deg2rad([ -90,  90;
            0,  85;
            -20,  45;
            -85,  15;
            -180, 180]);
        J1_OFFSET = deg2rad(  0);
        J2_OFFSET = deg2rad(-95);
        J3_OFFSET = deg2rad( 90);
        J4_OFFSET = deg2rad(  5);
        J5_OFFSET = deg2rad(  0);
        Z_MOVE_OFFSET = 0.025;
        
        %% Variables
        model;
        workspace;
    end
    
    methods(Access=private)
        %% Dobot Robot Constructor
        function self = SimulatedDobot()
            self.workspace = WorkspaceInterface.instance().GetWorkspace();
            self.GetDobotRobot();
            self.PlotAndColourRobot();
        end
    end
    
    methods
        %% Define the DH parameters of a Dobot robot
        function GetDobotRobot(self)
            
            % Define each of the six links of the robot
            L1 = Link('d',0.1040,'a',0.0000,'alpha',-pi/2,'offset', self.J1_OFFSET,'qlim',[self.QLIMS(1,1),self.QLIMS(1,2)]);
            L2 = Link('d',0.0000,'a',0.1427,'alpha',  0.0,'offset', self.J2_OFFSET,'qlim',[self.QLIMS(2,1),self.QLIMS(2,2)]);
            L3 = Link('d',0.0000,'a',0.1620,'alpha',  0.0,'offset', self.J3_OFFSET,'qlim',[self.QLIMS(3,1),self.QLIMS(3,2)]);
            L4 = Link('d',0.0000,'a',0.0600,'alpha',-pi/2,'offset', self.J4_OFFSET,'qlim',[self.QLIMS(4,1),self.QLIMS(4,2)]);
            L5 = Link('d',0.0680,'a',0.0000,'alpha',  0.0,'offset', self.J5_OFFSET,'qlim',[self.QLIMS(5,1),self.QLIMS(5,2)]);
            
            % Create a model of the Dobot
            self.model = SerialLink([L1 L2 L3 L4 L5],'name',self.NAME);
            %self.model.base = self.model.base*self.BASE_POSE;
        end
        
        %% PlotAndColourRobot
        function PlotAndColourRobot(self)
            
            % Import the appropriate ply model for each link of the robot
            for linkIndex = 0:self.model.n
                [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['dobotl',num2str(linkIndex),'.ply'],'tri');
                % Assign face data for the current link
                self.model.faces{linkIndex+1} = faceData;
                % Assign vertex data for the current link
                self.model.points{linkIndex+1} = vertexData;
            end
            
            % Display the robot in the workspace
            self.model.plot3d(zeros(1,self.model.n),'workspace',self.workspace);
            % Illuminate the workspace
            if isempty(findobj(get(gca,'Children'),'Type','Light'))
                camlight
            end
            
            % Implement no delay for faster animations
            self.model.delay = 0;
            
            % Import colours for each robot link
            for linkIndex = 0:self.model.n
                handles = findobj('Tag', self.model.name);
                h = get(handles,'UserData');
                try
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                        , plyData{linkIndex+1}.vertex.green ...
                        , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
            
            % Plot the dobot environment and the safety features
            hold on;
            DobotEnvironment.instance().AddObject('enclosure.ply','enclosure',eye(4));
            DobotEnvironment.instance().AddObject('simpleestop.ply','estop',eye(4)*transl(0.45,-0.34,-0.1));
            camlight;
        end
        
        %% Basic methods for the interface
        
        % angles: [base, foreArm, rearArm, endEffectorServo] in rad
        function angles = GetJointAngles(self)
            angles = self.model.getpos();
            angles(4) = [];
        end
        
        % position: [x,y,z coords]
        function SetCartPosition(self, position)
            x = position(1);
            y = position(2);
            z = position(3);
            position = transl(x,y,z)*trotx(pi);
            angles = self.model.ikcon(position);
            self.Move(angles);
        end
        
        % position: [x,y,z coords]
        function position = GetTooltipPosition(self)
            position = self.model.fkine(self.model.getpos());
        end
        
        % set single joint to a specific angle
        function SetSingleJointAngle(self, jointIndex, angle)
            angle = deg2rad(angle);
            jointAngles = self.GetJointAngles();
            jointAngles(jointIndex) = angle;
            q1 = jointAngles(1,1);
            q2 = jointAngles(1,2);
            q3 = jointAngles(1,3);
            q4 = jointAngles(1,4);
            q5 = q4;
            q4 = -(2*pi-(pi-q2)-(3/4*pi-q3)-pi/4);
            self.Move([q1 q2 q3 q4 q5]);
        end
        
        % updateQ4
        function UpdateQ4(self)
            jointAngles = self.GetJointAngles();
            q1 = jointAngles(1,1);
            q2 = jointAngles(1,2);
            q3 = jointAngles(1,3);
            q4 = jointAngles(1,4);
            q5 = q4;
            q4 = -(2*pi-(pi-q2)-(3/4*pi-q3)-pi/4);
            self.Move([q1 q2 q3 q4 q5]);
        end
        
        % set joints to a specific angle
        function SetJointAngles(self, angles)
            q1 = angles(1,1);
            q2 = angles(1,2);
            q3 = angles(1,3);
            q4 = angles(1,4);
            q5 = q4;
            q4 = -(2*pi-(pi-q2)-(3/4*pi-q3)-pi/4);
            self.Move([q1 q2 q3 q4 q5]);
        end
        
        % move a single joint by specific increment
        function IncrementJointByDelta(self, jointIndex, deltaQ)
            jointAngles = self.GetJointAngles();
            newAngle = rad2deg(jointAngles(jointIndex) + deg2rad(deltaQ));
            self.SetSingleJointAngle(jointIndex, newAngle);
        end
        
        % position: [x,y,z coords]
        function IncrementCartByDelta(self, deltaXYZ)
            x = deltaXYZ(1);
            y = -deltaXYZ(2);
            z = -deltaXYZ(3);
            position = self.GetTooltipPosition();
            position = position * transl(x, y, z);
            angles = self.model.ikcon(position);
            self.Move(angles);
        end
        
        % home the Dobot
        function Home(self)
            self.Move(zeros(1,5));
        end
        
        % Refine a joint trajectory to within a specific angle per step
        function trajectory = GetTrajectory(self,goalQ)
            angle = 1;
            steps = 20;
            while ~isempty(find(angle < abs(diff(rad2deg(jtraj(self.model.getpos(), goalQ, steps)))),1))
                steps = steps + 1;
            end
            trajectory = jtraj(self.model.getpos(), goalQ, steps);
        end
        
        % Perform collision avoidance through user input
        function collision = AvoidCollision(~,objectHitIndex)
            dlgTitle    = 'Collision!';
            dlgQuestion = 'Remove object and continue?';
            choice = questdlg(dlgQuestion,dlgTitle,'Yes','No', 'Yes');
            if strcmp(choice, 'Yes')
                DobotEnvironment.instance().DeleteObject(objectHitIndex)
                CollisionChecker.instance().DeleteCollisionPoint()
                collision = 0;
            else
                collision = 1;
                CollisionChecker.instance().DeleteCollisionPoint()
            end
        end
        
        
        % Move to a specified joint configuration
        function Move(self, goalQ, graspObject)
            collision = 0;
            moveWithObjectFlag = 0;
            
            if 2 < nargin
                moveWithObjectFlag = 1;
            end
            
            % Determine a trajectory between joint states and check each
            % object in the environment for a line-plane intersection along
            % the returned trajectory.
            trajectory = GetTrajectory(self,goalQ);
            objectsToCheck = DobotEnvironment.instance().GetObjects();
            for checkObj = 1:1:length(objectsToCheck)
                [vertex,faces,faceNormals] = objectsToCheck{checkObj}.GetModelData();
                collision = CollisionChecker.instance().IsCollision(self.model,trajectory,faces,vertex,faceNormals);
                if collision == 1
                    objectHitIndex = checkObj;
                    break
                end
            end
            
            % Check for collisions
            if collision == 1
                collision = self.AvoidCollision(objectHitIndex);
            end
            
            if collision == 0
                % Animate the Dobot at each joint position in the trajectory path
                for trajStep = 1:size(trajectory,1)
                    % Check status of the safety features before moving
                    if SafetyFeatures.instance().GetStatus()
                        % Wait until it is safe to move
                        while SafetyFeatures.instance().GetStatus()
                        end
                    end
                    % Execute movement
                    newQ = trajectory(trajStep,:);
                    self.model.animate(newQ);
                    % Update the pose of the grasped part
                    if moveWithObjectFlag == 1
                        newPose = self.model.fkine(newQ);
                        graspObject.MoveBrick(newPose * trotx(pi));
                    end
                    pause(0.08);
                end
            end
        end
        
        % Return the inverse kinematics of the dobot model
        function jointAngles = InverseKinematics(self, position)
            jointAngles = self.model.ikcon(position);
            jointAngles(4) = [];
        end
        
        % Plot a single joint state with collision detection
        function PlotSingleJointAngle(self,jointIndex,angle)
            angle = deg2rad(angle);
            jointAngles = self.GetJointAngles();
            jointAngles(jointIndex) = angle;
            q1 = jointAngles(1,1);
            q2 = jointAngles(1,2);
            q3 = jointAngles(1,3);
            q4 = jointAngles(1,4);
            q5 = q4;
            q4 = -(2*pi-(pi-q2)-(3/4*pi-q3)-pi/4);
            goalQ = [q1 q2 q3 q4 q5];
            
            % Determine a trajectory between joint states and check each
            % object in the environment for a line-plane intersection along
            % the returned trajectory.
            trajectory = GetTrajectory(self,goalQ);
            objectsToCheck = DobotEnvironment.instance().GetObjects();
            for checkObj = 1:1:length(objectsToCheck)
                [vertex,faces,faceNormals] = objectsToCheck{checkObj}.GetModelData();
                collision = CollisionChecker.instance().IsCollision(self.model,trajectory,faces,vertex,faceNormals);
                if collision == 1
                    objectHitIndex = checkObj;
                    break
                end
            end
            
            % Check for collisions
            if collision == 1
                collision = self.AvoidCollision(objectHitIndex);
            end
            
            if collision == 0
                % Check the status of the safety features before moving
                if ~SafetyFeatures.instance().GetStatus()
                    self.model.animate([q1 q2 q3 q4 q5]);
                    newNewQ(1:3) = goalQ(1:3);
                    newNewQ(4)= goalQ(5);
                    DobotRosInterface.instance().SetJointAngles(newNewQ);
                end
            end
        end
        
        % Pick and place a set of objects to a set of end points
        function PickAndPlace(self, objectList, endPoints)
            
            numObjects = length(objectList);
            offsetTr = zeros(4,4);
            offsetTr(3,4) = self.Z_MOVE_OFFSET;
            
            for i = 1:1:numObjects
                % Raise the z height to avoid collision with previous layer
                placeOffset = endPoints(:,:,i);
                placeOffset = placeOffset(3,4);
                
                %% Pick Object
                % Stage 1 - raised above object
                goalPose = (objectList{i}.GetGraspPose() + offsetTr) * trotx(pi);
                goalQ = self.model.ikcon(goalPose);
                self.Move(goalQ);
                
                % Stage 2 - lower onto object
                goalPose = (objectList{i}.GetGraspPose()) * trotx(pi);
                goalQ = self.model.ikcon(goalPose);
                self.Move(goalQ);
                
                % Stage 3 - grasp and raise object upwards
                goalPose = (objectList{i}.GetGraspPose() + placeOffset) * trotx(pi);
                goalQ = self.model.ikcon(goalPose);
                self.Move(goalQ,objectList{i});
                
                %% Place Object
                % Stage 1 - move the object to just above the placement location
                goalPose = endPoints(:,:,i) + offsetTr;
                goalQ = self.model.ikcon(goalPose);
                self.Move(goalQ,objectList{i});
                
                % Stage 2 - lower object into placement location
                goalPose = endPoints(:,:,i);
                goalQ = self.model.ikcon(goalPose);
                self.Move(goalQ,objectList{i});
                
                % Stage 3 - release object and raise upwards
                goalPose = endPoints(:,:,i) + offsetTr;
                goalQ = self.model.ikcon(goalPose);
                self.Move(goalQ);
            end
        end
    end
    
    %% Internal method for singleton implementation
    methods(Static)
        function self = instance()
            persistent uniqueInstance
            if isempty(uniqueInstance)
                self = SimulatedDobot();
                uniqueInstance = self;
            else
                self = uniqueInstance;
            end
        end
    end
end