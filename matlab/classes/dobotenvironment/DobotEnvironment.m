%% This interface inherits the Singleton
%% The only instance of that class is obtainable by calling 
%% DobotEnvironment.instance()
%%
%% Contains objects in the Dobots environment
%%
%% Author: Simon Fryc

%% Class definition
classdef DobotEnvironment < Singleton
    
    properties % Public Access
        
        %% Variables
        objects; % object container
    end
    
    methods(Access=private)
        %% Constructor
        function self = DobotEnvironment()
        end
    end
    
    methods
        %% Add object
        function AddObject(self, partFile, name, base)
            self.objects{length(self.objects)+1} = WorldObject(partFile,name,base);          
        end
        
        %% Delete object
        function DeleteObject(self,objectIndexOrName)
            if isnumeric(objectIndexOrName)
                delete(self.objects{objectIndexOrName}.mesh);
                self.objects(objectIndexOrName) = [];
            else
                for i = 1:1:length(self.objects)
                    if strcmp(self.objects{i}.name,objectIndexOrName)                         
                        delete(self.objects{i}.mesh);
                        self.objects(i) = [];
                        break
                    end
                end
            end
        end
        
        %% Return all objects in the environment
        function objects = GetObjects(self)
            objects = self.objects;
        end
    end
    
    %% Internal method for singleton implementation
    methods(Static)
        function self = instance()
            persistent uniqueInstance
            if isempty(uniqueInstance)
                self = DobotEnvironment();
                uniqueInstance = self;
            else
                self = uniqueInstance;
            end
        end
    end
end