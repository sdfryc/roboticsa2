%% This interface inherits the Singleton
%% The only instance of that class is obtainable by calling 
%% WorkspaceInterface.instance()
%%
%% Contains a workspace interface
%%
%% Author: Simon Fryc

%% Class definition
classdef WorkspaceInterface < Singleton
    
    properties % Public Access               
        %% Variables
        workspaceObject = [-0.5 0.5 -0.5 0.5 -0.1 0.8];
    end
    
    methods(Access=private)
        % Constructor has to be private 
        function self = WorkspaceInterface()
        end
    end
   
    %% Basic methods
    methods % Public Access
        function SetWorkspace(self, workspace)
            self.workspaceObject = workspace;
        end
        
        function workspace = GetWorkspace(self)
            workspace = self.workspaceObject;
        end
    end
    
    %% Internal method for singleton implementation
    methods(Static)
      function self = instance()
         persistent uniqueInstance
         if isempty(uniqueInstance)
            self = WorkspaceInterface();
            uniqueInstance = self;
         else
            self = uniqueInstance;
         end
      end
   end
end

