%% non relevant
function varargout = cameraUi(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cameraUi_OpeningFcn, ...
                   'gui_OutputFcn',  @cameraUi_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

%% relevant
function cameraUi_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;



%plot(handles.image_fig,[1 2 3],[1 2 3])
%image = imread('button.jpg');
image = WebcamInterface.instance().GetImage();
imshow(image, 'Parent', handles.image_fig);

% timer
handles.pollStatusTimer = timer('BusyMode', 'queue', 'ExecutionMode',...
                                'fixedRate', 'Period', 0.25);
set(handles.pollStatusTimer, 'TimerFcn', {@onPollStatusTimerTick, hObject});
start(handles.pollStatusTimer);

% Update handles structure
guidata(hObject, handles);

function onPollStatusTimerTick(hObject, eventdata, parent_GUI)
    handles = guidata(parent_GUI);
    image = WebcamInterface.instance().GetImage();
    imshow(image, 'Parent', handles.image_fig);

function update_btn_Callback(hObject, eventdata, handles)
image = WebcamInterface.instance().GetImage();
imshow(image, 'Parent', handles.image_fig);

function close_btn_Callback(hObject, eventdata, handles)
WebcamInterface.instance().Close();

%% non relevant
function varargout = cameraUi_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function image_fig_CreateFcn(hObject, eventdata, handles)
