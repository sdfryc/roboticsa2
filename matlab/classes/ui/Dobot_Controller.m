%-----------%
% GUI Setup %
%-----------%

function varargout = Dobot_Controller(varargin)
% DOBOT_CONTROLLER MATLAB code for Dobot_Controller.fig
%      DOBOT_CONTROLLER, by itself, creates a new DOBOT_CONTROLLER or raises the existing
%      singleton*.
%
%      H = DOBOT_CONTROLLER returns the handle to a new DOBOT_CONTROLLER or the handle to
%      the existing singleton*.
%
%      DOBOT_CONTROLLER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DOBOT_CONTROLLER.M with the given input arguments.
%
%      DOBOT_CONTROLLER('Property','Value',...) creates a new DOBOT_CONTROLLER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Dobot_Controller_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Dobot_Controller_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Dobot_Controller

% Last Modified by GUIDE v2.5 16-Oct-2018 03:17:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Dobot_Controller_OpeningFcn, ...
    'gui_OutputFcn',  @Dobot_Controller_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
% --- Executes just before Dobot_Controller is made visible.
function Dobot_Controller_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Dobot_Controller (see VARARGIN)

% Set the emergency toggle button image
set(handles.eStopToggle, 'CData', imread('estopimg.png'));

% Set the light curtain toggle button image
set(handles.lightCurtainToggle, 'CData', imread('lightcurtatinimg.png'));

% Set the current axes to gui axes1
handles.axes1 = gca;

% Create a Dobot interface
handles.robot = SimulatedDobot.instance();
view(100,20);
UpdateJointControlLabels(hObject, eventdata, handles);

% Choose default command line output for Dobot_Controller
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% --- Outputs from this function are returned to the command line.
function varargout = Dobot_Controller_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%-----------------%
% Slider Elememts %
%-----------------%

% --- Executes on slider movement.
function j1Slider_Callback(hObject, eventdata, handles)
handles.robot.PlotSingleJointAngle(1,get(hObject,'Value'));
UpdateJointControlLabels(hObject, eventdata, handles);
% --- Executes on slider movement.
function j2Slider_Callback(hObject, eventdata, handles)
handles.robot.PlotSingleJointAngle(2,get(hObject,'Value'));
UpdateJointControlLabels(hObject, eventdata, handles);
% --- Executes on slider movement.
function j3Slider_Callback(hObject, eventdata, handles)
handles.robot.PlotSingleJointAngle(3,get(hObject,'Value'));
UpdateJointControlLabels(hObject, eventdata, handles);
% --- Executes on slider movement.
function j4Slider_Callback(hObject, eventdata, handles)
handles.robot.PlotSingleJointAngle(4,get(hObject,'Value'));
UpdateJointControlLabels(hObject, eventdata, handles);


%--------------------%
% Cartesian Elememts %
%--------------------%

% --- Executes on button press in xNegButton.
function xNegButton_Callback(hObject, eventdata, handles)
handles.robot.IncrementCartByDelta([-GetCartResolution(hObject, eventdata, handles), 0, 0])
UpdateJointControlLabels(hObject, eventdata, handles);
ResetSliderValues(hObject, eventdata, handles);
% --- Executes on button press in yNegButton.
function yNegButton_Callback(hObject, eventdata, handles)
handles.robot.IncrementCartByDelta([0, -GetCartResolution(hObject, eventdata, handles) 0])
UpdateJointControlLabels(hObject, eventdata, handles);
ResetSliderValues(hObject, eventdata, handles);
% --- Executes on button press in zNegButton.
function zNegButton_Callback(hObject, eventdata, handles)
handles.robot.IncrementCartByDelta([0, 0, -GetCartResolution(hObject, eventdata, handles)])
UpdateJointControlLabels(hObject, eventdata, handles);
ResetSliderValues(hObject, eventdata, handles);
% --- Executes on button press in xPosButton.
function xPosButton_Callback(hObject, eventdata, handles)
handles.robot.IncrementCartByDelta([GetCartResolution(hObject, eventdata, handles), 0, 0])
UpdateJointControlLabels(hObject, eventdata, handles);
ResetSliderValues(hObject, eventdata, handles);
% --- Executes on button press in yPosButton.
function yPosButton_Callback(hObject, eventdata, handles)
handles.robot.IncrementCartByDelta([0, GetCartResolution(hObject, eventdata, handles) 0])
UpdateJointControlLabels(hObject, eventdata, handles);
ResetSliderValues(hObject, eventdata, handles);
% --- Executes on button press in zPosButton.
function zPosButton_Callback(hObject, eventdata, handles)
handles.robot.IncrementCartByDelta([0, 0, GetCartResolution(hObject, eventdata, handles)])
UpdateJointControlLabels(hObject, eventdata, handles);
ResetSliderValues(hObject, eventdata, handles);


%--------------------------%
%  Safety Elememts %
%--------------------------%

% --- Executes on button press in lightCurtainToggle.
function lightCurtainToggle_Callback(hObject, eventdata, handles)
if get(hObject,'Value')
    LightCurtain.instance().Show();
else
    LightCurtain.instance().Hide();
end
% --- Executes on button press in eStopToggle.
function eStopToggle_Callback(hObject, eventdata, handles)
if get(hObject,'Value')
    SafetyFeatures.instance().Stop();
    msgbox('eStop pressed!') 
    %set(handles.activeLabel, 'Visible', 'off');
else
    SafetyFeatures.instance().Reset();
    msgbox('eStop deactivated!')
    ResetSliderValues(hObject, eventdata, handles);
    %set(handles.activeLabel, 'Visible', 'on');
end


%----------------------%
%  Simulation Elememts %
%----------------------%

% --- Executes on button press in homeButton.
function homeButton_Callback(hObject, eventdata, handles)
% hObject    handle to homeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.robot.Home();
UpdateJointControlLabels(hObject, eventdata, handles);
% --- Executes on button press in buildWallButton.
function buildWallButton_Callback(hObject, eventdata, handles)
% hObject    handle to buildWallButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.robot.Home();
UpdateJointControlLabels(hObject, eventdata, handles);
SetJointControlLabels(hObject, eventdata, handles,'off');

numberOfBricks = 6;
centerOfBricks = [0.22, -0.15, 3/4 * pi];
wallOrigin = [0.22 0.10 3/4*pi];
DemoDobot.instance().WallBuilding(numberOfBricks,centerOfBricks,wallOrigin);
SetJointControlLabels(hObject, eventdata, handles,'on');
% --- Executes on button press in collisionButton.
function collisionButton_Callback(hObject, eventdata, handles)
% hObject    handle to collisionButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.robot.Home();
SetJointControlLabels(hObject, eventdata, handles,'off');
UpdateJointControlLabels(hObject, eventdata, handles);
DemoDobot.instance().ForcedCollision();
SetJointControlLabels(hObject, eventdata, handles,'on');


%-------------------%
% General Functions %
%-------------------%

% --- Updates joint controller labels as joints are moved
function UpdateJointControlLabels(hObject, eventdata, handles)

joints = rad2deg(handles.robot.GetJointAngles());
set(handles.j1Label, 'String', joints(1));
set(handles.j2Label, 'String', joints(2));
set(handles.j3Label, 'String', joints(3));
set(handles.j4Label, 'String', joints(4));

tr = handles.robot.GetTooltipPosition();
set(handles.xPosLabel, 'String', tr(1,4));
set(handles.yPosLabel, 'String', tr(2,4));
set(handles.zPosLabel, 'String', tr(3,4));

rpy = round(tr2rpy(tr),3);
set(handles.rollLabel,  'String', rpy(1));
set(handles.pitchLabel, 'String', rpy(2));
set(handles.yawLabel,   'String', rpy(3));
% --- Updates joint controller labels as joints are moved
function SetJointControlLabels(hObject, eventdata, handles,value)
set(handles.j1Label, 'Enable', value);
set(handles.j2Label, 'Enable', value);
set(handles.j3Label, 'Enable', value);
set(handles.j4Label, 'Enable', value);;
set(handles.xPosLabel, 'Enable', value);
set(handles.yPosLabel, 'Enable', value);
set(handles.zPosLabel,  'Enable', value);
set(handles.rollLabel, 'Enable', value);
set(handles.pitchLabel, 'Enable', value);
set(handles.yawLabel, 'Enable', value);
% --- Resets Slider Values
function ResetSliderValues(hObject, eventdata, handles)
joints = rad2deg(handles.robot.GetJointAngles());
set(handles.j1Slider, 'Value', joints(1));
set(handles.j2Slider, 'Value', joints(2));
set(handles.j3Slider, 'Value', joints(3));
set(handles.j4Slider, 'Value', joints(4));
% --- Get cartesian resolution
function resolution = GetCartResolution(hObject, eventdata, handles)
if get(handles.fineButton, 'Value')
    resolution = 0.001;
elseif get(handles.mediumButton, 'Value')
    resolution = 0.01;
else
    resolution = 0.1;
end


%-----------------%
% Unused Elememts %
%-----------------%

% --- Executes during object creation, after setting all properties.
function j1Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to j1Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
% --- Executes during object creation, after setting all properties.
function j2Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to j2Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
% --- Executes during object creation, after setting all properties.
function j3Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to j3Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
% --- Executes during object creation, after setting all properties.
function j4Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to j4Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2
