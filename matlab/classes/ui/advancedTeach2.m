%% UI
function varargout = advancedTeach2(varargin)
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @advancedTeach2_OpeningFcn, ...
                       'gui_OutputFcn',  @advancedTeach2_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT


% --- Executes just before advancedTeach2 is made visible.
function advancedTeach2_OpeningFcn(hObject, eventdata, handles, varargin)
    handles.output = hObject;
    
    % Add code here
    handles.pollStatusTimer = timer('BusyMode', 'queue', 'ExecutionMode',...
                                    'fixedRate', 'Period', 1.0);
    set(handles.pollStatusTimer, 'TimerFcn', {@onPollStatusTimerTick, hObject});
    start(handles.pollStatusTimer);
    
    % Update handles structure
    guidata(hObject, handles);


function onPollStatusTimerTick(hObject, eventdata, parent_GUI)
    handles = guidata(parent_GUI);
    nowString = datestr(datetime('now'));
    set(handles.time_tv, 'String', nowString);


function figure1_CloseRequestFcn(hObject, eventdata, handles)
    stop(handles.pollStatusTimer);
    delete(hObject);


%% Buttons
% pump on
function pump_on_btn_Callback(hObject, eventdata, handles)
DobotRosInterface.instance().SetPumpOn();
% pump off
function pump_off_btn_Callback(hObject, eventdata, handles)
DobotRosInterface.instance().SetPumpOff();
% goto cam home
function cam_home_btn_Callback(hObject, eventdata, handles)
DobotRosInterface.instance().GoToCameraHome();
%% Move cartesian
% move x
function move_x_inc_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_x_et,'String'));
DobotRosInterface.instance().IncrementPositionByDelta([etValue 0 0]);
function move_x_dec_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_x_et,'String'));
DobotRosInterface.instance().IncrementPositionByDelta([-etValue 0 0]);
% move y
function move_y_dec_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_y_et,'String'));
DobotRosInterface.instance().IncrementPositionByDelta([0 etValue 0]);
function move_y_inc_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_y_et,'String'));
DobotRosInterface.instance().IncrementPositionByDelta([0 -etValue 0]);
% move z
function move_z_inc_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_z_et,'String'));
DobotRosInterface.instance().IncrementPositionByDelta([0 0 etValue]);
function move_z_dec_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_z_et,'String'));
DobotRosInterface.instance().IncrementPositionByDelta([0 0 -etValue]);
%% Move joints
% Move j0
function move_j0_inc_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_j0_et,'String'))*pi/180;
DobotRosInterface.instance().IncrementJointByDelta(1,etValue);
function move_j0_dec_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_j0_et,'String'))*pi/180;
DobotRosInterface.instance().IncrementJointByDelta(1,-etValue);
% Move j1
function move_j1_inc_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_j1_et,'String'))*pi/180;
DobotRosInterface.instance().IncrementJointByDelta(2,etValue);
function move_j1_dec_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_j2_et,'String'))*pi/180;
DobotRosInterface.instance().IncrementJointByDelta(2,-etValue);
% Move j2
function move_j2_inc_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_j2_et,'String'))*pi/180;
DobotRosInterface.instance().IncrementJointByDelta(3,etValue);
function move_j2_dec_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_j2_et,'String'))*pi/180;
DobotRosInterface.instance().IncrementJointByDelta(3,-etValue);
% Move j3
function move_j3_inc_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_j3_et,'String'))*pi/180;
DobotRosInterface.instance().IncrementJointByDelta(4,etValue);
function move_j3_dec_btn_Callback(hObject, eventdata, handles)
etValue = eval(get(handles.move_j3_et,'String'))*pi/180;
DobotRosInterface.instance().IncrementJointByDelta(4,-etValue);


%% Non relevant
function move_z_et_Callback(hObject, eventdata, handles)
function pump_on_btn_ButtonDownFcn(hObject, eventdata, handles)
function move_z_et_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function move_y_et_Callback(hObject, eventdata, handles)
function move_y_et_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function move_x_et_Callback(hObject, eventdata, handles)
function move_x_et_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function move_j3_et_Callback(hObject, eventdata, handles)
function move_j3_et_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function move_j2_et_Callback(hObject, eventdata, handles)
function move_j2_et_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function move_j1_et_Callback(hObject, eventdata, handles)
function move_j1_et_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function move_j0_et_Callback(hObject, eventdata, handles)
function move_j0_et_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Outputs from this function are returned to the command line.
function varargout = advancedTeach2_OutputFcn(hObject, eventdata, handles)



