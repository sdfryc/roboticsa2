function varargout = loggerUi2(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @loggerUi2_OpeningFcn, ...
                   'gui_OutputFcn',  @loggerUi2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

function loggerUi2_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;

% !Set the position!
set(handles.figure1,'Units', 'normalized');
set(handles.figure1,'Position', [0.5 0.5 0.35 0.2]);
guidata(hObject, handles);

function varargout = loggerUi2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in log_btn.
function log_btn_Callback(hObject, eventdata, handles)
% hObject    handle to log_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tagString = get(handles.tag_et,'String');
msgString = get(handles.msg_et,'String');
LoggerInterface.instance().Log(tagString, msgString);



function msg_et_Callback(hObject, eventdata, handles)
% hObject    handle to msg_et (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of msg_et as text
%        str2double(get(hObject,'String')) returns contents of msg_et as a double


% --- Executes during object creation, after setting all properties.
function msg_et_CreateFcn(hObject, eventdata, handles)
% hObject    handle to msg_et (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tag_et_Callback(hObject, eventdata, handles)
% hObject    handle to tag_et (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tag_et as text
%        str2double(get(hObject,'String')) returns contents of tag_et as a double


% --- Executes during object creation, after setting all properties.
function tag_et_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tag_et (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
