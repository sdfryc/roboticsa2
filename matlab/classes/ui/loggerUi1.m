function varargout = loggerUi1(varargin)
% LOGGERUI1 MATLAB code for loggerUi1.fig
%      LOGGERUI1, by itself, creates a new LOGGERUI1 or raises the existing
%      singleton*.
%
%      H = LOGGERUI1 returns the handle to a new LOGGERUI1 or the handle to
%      the existing singleton*.
%
%      LOGGERUI1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LOGGERUI1.M with the given input arguments.
%
%      LOGGERUI1('Property','Value',...) creates a new LOGGERUI1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before loggerUi1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to loggerUi1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help loggerUi1

% Last Modified by GUIDE v2.5 10-Oct-2018 16:13:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @loggerUi1_OpeningFcn, ...
                   'gui_OutputFcn',  @loggerUi1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before loggerUi1 is made visible.
function loggerUi1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to loggerUi1 (see VARARGIN)

% Choose default command line output for loggerUi1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes loggerUi1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = loggerUi1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in log_btn.
function log_btn_Callback(hObject, eventdata, handles)
% hObject    handle to log_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tagString = get(handles.tag_et,'String');
msgString = get(handles.msg_et,'String');
LoggerInterface.instance().Log(tagString, msgString);



function msg_et_Callback(hObject, eventdata, handles)
% hObject    handle to msg_et (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of msg_et as text
%        str2double(get(hObject,'String')) returns contents of msg_et as a double


% --- Executes during object creation, after setting all properties.
function msg_et_CreateFcn(hObject, eventdata, handles)
% hObject    handle to msg_et (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tag_et_Callback(hObject, eventdata, handles)
% hObject    handle to tag_et (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tag_et as text
%        str2double(get(hObject,'String')) returns contents of tag_et as a double


% --- Executes during object creation, after setting all properties.
function tag_et_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tag_et (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
