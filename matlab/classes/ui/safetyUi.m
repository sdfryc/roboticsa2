%% non relevant
function varargout = safetyUi(varargin)
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @safetyUi_OpeningFcn, ...
                       'gui_OutputFcn',  @safetyUi_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end


%% relevant
function safetyUi_OpeningFcn(hObject, eventdata, handles, varargin)
    handles.output = hObject;
    
    % set emergeny button image
    k = 0.85;
    [x, ~] = imread('button.jpg');
    imageResized = imresize(x, [100*k 100*k]);
    set(handles.pushbutton1, 'CData', imageResized);
   
    % ArduinoInterface
    handles.arduinoInterface = ArduinoInterfaceStub.instance();
    
    % SafetyManager
    handles.safetyManager = SafetyManager.instance();
    
    % Timer
    handles.pollStatusTimer = timer('BusyMode', 'queue', ...
                                    'ExecutionMode', 'fixedRate', ...
                                    'Period', 0.5, ...
                                    'StartDelay', 0.0);
    set(handles.pollStatusTimer, 'TimerFcn', {@onTimerTick, hObject});
    

    % Timer start
    start(handles.pollStatusTimer);
    
    % Update handles structure   
    guidata(hObject, handles);
    
    
function figure1_CloseRequestFcn(hObject, eventdata, handles)
stop(handles.pollStatusTimer);
delete(hObject);

% timer tick code goes here
function onTimerTick(hObject, eventdata, parent_GUI)
    handles = guidata(parent_GUI);
    try
        nowString = datestr(datetime('now'));
        set(handles.time_tv, 'String', nowString);
        
        lightCurtainStatus = handles.arduinoInterface.GetLightCurtainStatus();
        set(handles.light_curtain_tv, 'String', lightCurtainStatus);
        
        set(handles.safety_status_tv, 'String', handles.safetyManager.GetSafetyStatus());
        set(handles.emergency_btn_tv, 'String', handles.safetyManager.GetEmergencyButtonStatus());
            
    catch
    end

% TOOGLE EMERGENCY BUTTON
function pushbutton1_Callback(hObject, eventdata, handles)
    handles.safetyManager.ToggleEmergencyButton();

% RESTART ARDUINO BUTTON
function pushbutton2_Callback(hObject, eventdata, handles)
    handles.arduinoInterface.Restart();

%% non relevant
function varargout = safetyUi_OutputFcn(hObject, eventdata, handles)
    varargout{1} = handles.output;
