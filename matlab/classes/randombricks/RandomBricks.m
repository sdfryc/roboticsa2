%% Creates a pile of semi-random bricks
%% Author: Simon Fryc

%% Class definition
classdef RandomBricks < handle
    
    properties
        brickCount = 6;                     % The number of bricks to be created
        centre = [0.2, 0, 0];               % The centre coordinates of brick pile
        brick;                              % A cell structure of brick models
        areaSize = [0.1, 0.3];              % Rectengular area brick may exist inside
        dimensions = [0.042, 0.025, 0.019]; % Brick dimensions (LxWxH)
    end
    
    methods
        %% Constructor
        function self = RandomBricks(brickCount, centre)
            if 0 < nargin
                self.brickCount = brickCount;
            end
            if 1 < nargin
                self.centre = centre;
            end
            
            % Determine an even spacing interval between bricks to prevent
            % intersections from occuring.
            xCentre = self.centre(1);
            yCentre = self.centre(2);
            zRotate = self.centre(3);
            
            % Algorithm setup variables
            brickInterval = self.areaSize(2) / self.brickCount;
            stepInterval = brickInterval / 2;
            startPosition = self.brickCount/2 * -brickInterval;
            localCentre = startPosition + stepInterval;
            
            % Create the required number of bricks
            for i = 1:self.brickCount
                % Random brick translation and rotation calculations
                xOffset = (2*rand()-1) * self.areaSize(1)/2;
                xPosition = xCentre + xOffset;
                
                yOffset = localCentre;
                yPosition = yCentre + yOffset;
                
                zRotation = (2*rand()-1) * 2 * pi;
                
                localOrigin = se2(xCentre,yCentre,0);
                localRotation = se2(0, 0, zRotate);
                localOriginOffset = se2(xOffset, yOffset, zRotation);;
                
                base = se3(localOrigin * localRotation * localOriginOffset);
                
                % Plot 3D model
                self.brick{i} = Brick(['brick',num2str(i)],base);
                pause(0.1);
                hold on;
                % Update centre locations for the next brick
                localCentre = localCentre + brickInterval;
            end
        end
        
        %% Return a pose for grasping the brick
        function graspPose = GetGraspPose(self,brickIndex)
            graspPose = self.brick{brickIndex}.base;
            graspPose(3,4) = graspPose(3,4) + self.dimensions(3);
        end
        
        %% Return the dimensions of the bricks
        function dimensions = GetDimensions(self)
            dimensions = self.dimensions;
        end
        
        %% Brick wall configuration solver for x number of bricks
        function poseList = ConfigureWall(self,wallOrigin)
            
            bricks = self.brickCount;   % The number of bricks
            bricksRemaining = bricks;   % The bricks left to place on wall
            TopRow = 1;                 % The number of bricks on the top row of the wall
            bricksRequired = 1;         % The number of bricks required to form the next row on the wall
            rows = [];                  % The number of bricks in each row of the wall
            poseList = [];              % A list of poses for each brick in the wall
            
            while 1
                % Check how many bricks are left to place on the wall
                if bricksRemaining == 0
                    % The wall is complete
                    break
                elseif bricksRemaining < bricksRequired
                    % There are not enough bricks to complete the next row.
                    % Increment the number of bricks on the top layer and
                    % try again.
                    TopRow = TopRow + 1;
                    
                    % Reset variables
                    bricksRequired = TopRow;
                    bricksRemaining = bricks;
                    rows = [];
                else
                    % There are enough bricks to form a new row on the
                    % wall. Subtract the number of bricks required for the row.
                    bricksRemaining = bricksRemaining - bricksRequired;
                    
                    % Store the number of bricks used for the row
                    rows(length(rows)+1) = bricksRequired;
                    
                    % Move to the next row
                    bricksRequired = bricksRequired + 1;
                end
            end
            
            % Transpose into a column vector
            rows = transpose(rows);
            
            % Algorithm setup variables
            brickLength = self.dimensions(1);
            brickHeight = self.dimensions(3);
            wallHeight = 0;
            totalRows = length(rows);
            brickIndex = 1;
            
            % Determine the position for each brick in the wall
            for thisRow = 1:1:totalRows
                centreTfm = se3(se2(wallOrigin(1),wallOrigin(2),wallOrigin(3)));
                offsetTfm = se3(se2((brickLength/2 * thisRow),0,0));
                centreTfm = centreTfm * offsetTfm;
                translationTfm = se3(se2(0,0,0));
                bricksInRow = rows(length(rows) - thisRow + 1);
                
                for brick = 1:1:bricksInRow
                    brickBase = centreTfm * translationTfm;
                    brickBase(3,4) = wallHeight + self.dimensions(3);
                    poseList(:,:,brickIndex) = brickBase * trotx(pi);
                    translationTfm(1,4) = translationTfm(1,4) + brickLength;
                    brickIndex = brickIndex + 1;
                end
                wallHeight = wallHeight + brickHeight;
            end
        end
        
        %% Return a list of brick objects
        function bricks = GetBricks(self)
            bricks = self.brick;
        end
        
        %% Delete all bricks
        function Delete(self)
            for i = 1:1:length(self.brick)
                self.brick{i}.Delete();
            end
        end
    end
end