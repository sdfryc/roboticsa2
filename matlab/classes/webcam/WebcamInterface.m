%% This interface inherits the Singleton
%% The only instance of that class is obtainable by calling 
%% WebcamInterface.instance()
%%
%% Autor: Moritz Oertel

classdef WebcamInterface < Singleton

    properties % Public Access
        %% Defines
        ROTATION_MATRIX = inv(eye(3)*rotz(pi/4)*roty(pi));
        %TRANSLATION_VECTOR = [20 -235 -240]'/1000;
        %TRANSLATION_VECTOR = [235 -20 240]/1000' - [20 -8 0]/1000'; % in mm
        TRANSLATION_VECTOR = [235 -20 240]'/1000
        
        
        RESOLUTION = '640x480';
        
        %% Variables
        cameraObject;
        cameraParams;
    end
   
    methods(Access=private)
        % Constructor has to be private 
        function self = WebcamInterface()
            self.cameraObject = webcam('Webcam C170');
            tmp = load('cameraParams');
            self.cameraParams = tmp.cameraParams;
            disp('Webcam UP');
        end
    end
    
    %% Basic methods
    methods % Public Access
        
        function Open(self)
            self.cameraObject = webcam;
        end

        function Close(self)
            try
                delete(self.cameraObject);
            catch
            end
        end
        function image = GetImage(self)
            if ~isvalid(self.cameraObject)
                self.Open()
            end
            if isvalid(self.cameraObject)
                image = snapshot(self.cameraObject);
            else
                error('Cannot reconnect to cam');
            end
            
        end
    end
    
    %% Transformation methods
    methods
        function worldPoints = ImagePointsToWorld(self, imagePoints)      
            worldPoints = pointsToWorld(self.cameraParams,...
                self.ROTATION_MATRIX,...
                self.TRANSLATION_VECTOR,...
                imagePoints);
        end
        
        function worldPoint = ImagePointToWorld2(self, imagePoint)
            u = imagePoint(1);
            v = imagePoint(2);
            f = 2.3/1000;
            u0 = 0.0;
            v0 = 0.0;
            z = 0.3;
            x = (u-u0)*z/f;
            y = (v-v0)*z/f;

            worldPoint = [x y z];
            
        end
    end
    
    %% Private methods
    methods (Access=private)
        
    end
    
    %% Internal method for singleton implementation
    methods(Static)
      function self = instance()
         persistent uniqueInstance
         if isempty(uniqueInstance)
            self = WebcamInterface();
            uniqueInstance = self;
         else
            self = uniqueInstance;
         end
      end
   end
end
