clc;
clear all;
close all;

% Define the workspace
workspace = [-0.4 0.4 -0.4 0.4 0 0.8];

% Define each of the six links of the robot
L1 = Link('d',0.1390,'a',0.0000,'alpha',-pi/2,'offset', 0.000);
L2 = Link('d',0.0000,'a',0.1427,'alpha',  0.0,'offset', -pi/2);
L3 = Link('d',0.0000,'a',0.1620,'alpha',  0.0,'offset',  pi/4);
L4 = Link('d',0.0000,'a',0.0525,'alpha',-pi/2,'offset',  pi/4);
L5 = Link('d',0.0680,'a',0.0000,'alpha',  0.0,'offset', -pi/2);

robot = SerialLink([L1 L2 L3 L4 L5],'name','myRobot');

% Import the appropriate ply model for each link of the UR3
for linkIndex = 0:robot.n
    [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['dobotl',num2str(linkIndex),'.ply'],'tri');
    % Assign face data for the current link
    robot.faces{linkIndex+1} = faceData;
    % Assign vertex data for the current link
    robot.points{linkIndex+1} = vertexData;
end

% Display the UR3 in the workspace
robot.plot3d(zeros(1,robot.n),'workspace',workspace);
% Illuminate the workspace
if isempty(findobj(get(gca,'Children'),'Type','Light'))
    camlight
end

% Implement no delay for faster animations
robot.delay = 0;

% Import colours for each UR3 link
for linkIndex = 0:robot.n
    handles = findobj('Tag', robot.name);
    h = get(handles,'UserData');
    try
        h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
            , plyData{linkIndex+1}.vertex.green ...
            , plyData{linkIndex+1}.vertex.blue]/255;
        h.link(linkIndex+1).Children.FaceColor = 'interp';
    catch ME_1
        disp(ME_1);
        continue;
    end
end

pause(0.1);

centerPoint = [ 0.0000,  0.0000,  0.025;
                0.0000,  0.0500,  0.000;
               -0.0713,  0.0000,  0.000;
               -0.0850, -0.0100,  0.000;
               -0.0251,  0.0000,  0.000;
                0.0000,  0.0000, -0.034];
           
radii = [0.10, 0.15, 0.05;
         0.10, 0.14, 0.14;
         0.15, 0.06, 0.06;
         0.12, 0.06, 0.06;
         0.06, 0.04, 0.04;
         0.02, 0.02, 0.03];
     
for i = 1:1:6
    [X,Y,Z] = ellipsoid(centerPoint(i,1), centerPoint(i,2), centerPoint(i,3), radii(i,1), radii(i,2), radii(i,3));
    robot.points{i} = [X(:),Y(:),Z(:)];
    warning off
    robot.faces{i} = delaunay(robot.points{i});
    warning on;
end

robot.plot3d([0,0,0,0,0]);
robot.teach();
hold on;

 % One side of the cube
[Y,Z] = meshgrid(-0.1:0.01:0.1,-0.1:0.01:0.1);
 sizeMat = size(Y);
 X = repmat(0.1,sizeMat(1),sizeMat(2));
 oneSideOfCube_h = surf(X,Y,Z);
 
 % Combine one surface as a point cloud
 cubePoints = [X(:),Y(:),Z(:)];
 
 % Make a cube by rotating the single side by 0,90,180,270, and around y to make the top and bottom faces
 cubePoints = [ cubePoints ...
              ; cubePoints * rotz(pi/2)...
              ; cubePoints * rotz(pi) ...
              ; cubePoints * rotz(3*pi/2) ...
              ; cubePoints * roty(pi/2) ...
              ; cubePoints * roty(-pi/2)];         
          
 % Plot the cube's point cloud         
 cubePoints = cubePoints + repmat([0.3,0,0.15],size(cubePoints,1),1);
 cube_h = plot3(cubePoints(:,1),cubePoints(:,2),cubePoints(:,3),'b.');

 algebraicDist = GetAlgebraicDist(cubePoints, centerPoint, radii);
 pointsInside = find(algebraicDist < 1);
 display(['There are ', num2str(size(pointsInside,1)),' points inside']);
 
 % Go through each ellipsoid
for i = 1:6
    cubePointsAndOnes = [inv(tr(:,:,i)) * [cubePoints,ones(size(cubePoints,1),1)]']';
    updatedCubePoints = cubePointsAndOnes(:,1:3);
    algebraicDist = GetAlgebraicDist(updatedCubePoints, centerPoint, radii);
    pointsInside = find(algebraicDist < 1);
    display(['2.10: There are ', num2str(size(pointsInside,1)),' points inside the ',num2str(i),'th ellipsoid']);
end
 %%
 function algebraicDist = GetAlgebraicDist(points, centerPoint, radii)

algebraicDist = ((points(:,1)-centerPoint(1))/radii(1)).^2 ...
              + ((points(:,2)-centerPoint(2))/radii(2)).^2 ...
              + ((points(:,3)-centerPoint(3))/radii(3)).^2;
end
