% Brick wall solver

clc;
bricks = 9;
bricksRemaining = bricks;
TopRow = 1;
count = 1;
rows = [];

while 1
    if bricksRemaining == 0
        disp('Solution Found!');
        break
    elseif bricksRemaining < count
        disp('Solution Not Found!');
        disp('Recalculating...');
        TopRow = TopRow + 1;
        count = TopRow;
        bricksRemaining = bricks;
        rows = [];
    else
        bricksRemaining = bricksRemaining - count;
        disp(count)
        rows(length(rows)+1) = count;
        count = count + 1;
    end
end

rows