%% Wall Building Algorithm NEW

clc;
close all;
clear all;

axis([-0.25 0.25 -0.25 0.25 0 0.25]);
hold on;

pile = Bricks2(15,[0 0]);
dim = pile.GetDimensions();
wallConfig = pile.ConfigureWall();

wallOrigin = [0 0 pi/6];
brickLength = dim(1);
brickHeight = dim(3);
wallHeight = 0;
totalRows = length(wallConfig);
brickCount = 1;

for thisRow = 1:1:totalRows	

    centreTfm = se3(se2(wallOrigin(1),wallOrigin(2),wallOrigin(3)));
    offsetTfm = se3(se2((brickLength/2 * thisRow),0,0));
    centreTfm = centreTfm * offsetTfm;
    translationTfm = se3(se2(0,0,0));
    bricksInRow = wallConfig(length(wallConfig) - thisRow + 1);
    
    for brick = 1:1:bricksInRow   
        pile.brick{brickCount}.base = centreTfm * translationTfm; 
        pile.brick{brickCount}.base(3,4) = wallHeight;        
        animate(pile.brick{brickCount},0);
        drawnow();
        translationTfm(1,4) = translationTfm(1,4) + brickLength;
		brickCount = brickCount + 1;
    end
	
	wallHeight = wallHeight + brickHeight;
    axis equal;
	
end