% Clear workspace variables
clc;
close all;

% Define the workspace dimensions
workspace = [-0.4 0.4 -0.4 0.4 0 0.8];
axis([-0.4 0.4 -0.4 0.4 -0.1 0.6]);
hold on;

% Define each of the six links of the robot
L1 = Link('d',0.1390,'a',0.0000,'alpha',-pi/2,'offset', 0.000);
L2 = Link('d',0.0000,'a',0.1427,'alpha',  0.0,'offset', -pi/2);
L3 = Link('d',0.0000,'a',0.1620,'alpha',  0.0,'offset',  pi/4);
L4 = Link('d',0.0000,'a',0.0525,'alpha',-pi/2,'offset',  pi/4);
L5 = Link('d',0.0680,'a',0.0000,'alpha',  0.0,'offset', -pi/2);

% Create the robot
robot = SerialLink([L1 L2 L3 L4 L5],'name','DoBot');
robot.teach();