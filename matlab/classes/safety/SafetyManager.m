%% This interface inherits the Singleton
%% The only instance of that class is obtainable by calling 
%% SafetyManager.instance()
%%
%% Autor: Moritz Oertel

classdef SafetyManager < Singleton

    properties % Public Access
        %% Defines
        % Safety Status
        STATUS_STOP = "STATUS_STOP";
        STATUS_OK = "STATUS_OK";
        % Emergency Button
        STATUS_INIT = "STATUS_INIT"
        STATUS_ENGAGED = "STATUS_ENGAGED";
        STATUS_DISENGAGED = "STATUS_DISENGAGED";
        
        %% Variables
        safetyStatus;
        emergencyButtonStatus;
    end
   
    
    %% Private constructor
    methods(Access=private)
        % Constructor has to be private 
        function self = SafetyManager()
            self.safetyStatus = self.STATUS_STOP;
            self.emergencyButtonStatus = self.STATUS_INIT;
            disp('SafetyManager UP');
        end
    end
    
    
    
    %% Basic methods
    methods % Public Access

        function ToggleEmergencyButton(self)
            if strcmp(self.emergencyButtonStatus, self.STATUS_INIT)
                self.emergencyButtonStatus = self.STATUS_DISENGAGED;
            elseif strcmp(self.emergencyButtonStatus, self.STATUS_ENGAGED)
                self.emergencyButtonStatus = self.STATUS_DISENGAGED;
            elseif strcmp(self.emergencyButtonStatus, self.STATUS_DISENGAGED)
                self.emergencyButtonStatus = self.STATUS_ENGAGED;
            end
        end
        
        function EngageEmergencyButton(self)
            self.emergencyButtonStatus = self.STATUS_ENGAGED;
        end
        
        function DisEngageEmergencyButton(self)
            self.emergencyButtonStatus = self.STATUS_DISENGAGED;
        end      
        
        function status = GetSafetyStatus(self)
            self.UpdateSafetyStatus();
            status = self.safetyStatus;
        end
        
        function emergencyButtonStatus = GetEmergencyButtonStatus(self)
            emergencyButtonStatus = self.emergencyButtonStatus;
        end
        
        function ResetEmergencyButton(self)
            self.emergencyButtonStatus = self.STATUS_INIT;
        end
    end
    
    %% Private methods
    methods (Access=private)
        function UpdateSafetyStatus(self)
            % Safety Logic goes here
            % .
            % ..
            % ...
            lightCurtainStatus = ArduinoInterfaceStub.instance().GetLightCurtainStatus();
            
            if lightCurtainStatus == ArduinoInterfaceStub.instance().STATUS_DETECTED
                self.safetyStatus = self.STATUS_STOP;
                return
            end
            
            if lightCurtainStatus == ArduinoInterfaceStub.instance().STATUS_STARTED
                self.safetyStatus = self.STATUS_STOP;
                return
            end
            
            if self.emergencyButtonStatus == self.STATUS_ENGAGED
                self.safetyStatus = self.STATUS_STOP;
                return
            end
            
            if self.emergencyButtonStatus == self.STATUS_DISENGAGED ...
                && lightCurtainStatus == ArduinoInterfaceStub.instance().STATUS_CLEAR
                % then:
                self.safetyStatus = self.STATUS_OK;
                return
            end
            
            self.safetyStatus = self.STATUS_STOP;
        end
    end
    
    %% Internal method for singleton implementation
    methods(Static)
      function self = instance()
         persistent uniqueInstance
         if isempty(uniqueInstance)
            self = SafetyManager();
            uniqueInstance = self;
         else
            self = uniqueInstance;
         end
      end
   end
end
