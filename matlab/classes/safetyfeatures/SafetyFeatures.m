%% An interface between the robot safety features and GUI window
%% Author: Simon Fryc

%% Class definition
classdef SafetyFeatures < Singleton
    
    properties
        %% Variables
        status;
        lastKeyPress;
        %% Defines        
        RESET = 0;
        STOP = 1;
        KEYBOARD_PAUSE = 0.01;
    end
    
    methods(Access=private)
        % Constructor has to be private
        function self = SafetyFeatures()
            self.status = self.RESET;
            self.lastKeyPress = self.RESET;
        end
    end
    
    %% Basic methods
    methods % Public Access
        function Stop(self)
            self.status = self.STOP;
        end
        
        function Reset(self)
            self.status = self.RESET;
        end
        
        function CheckKeyboard(self)
            pause(self.KEYBOARD_PAUSE);
            key = get(gcf,'CurrentKey');
            if(strcmp (key , 's') && self.lastKeyPress ~= self.STOP)
                self.status = self.STOP;
                disp('Emergency Stop Activated!');
                self.lastKeyPress = self.STOP;
            elseif(strcmp (key , 'r') && self.lastKeyPress ~= self.RESET)
                self.status = self.RESET;
                disp('Emergency Stop Reset.');
                self.lastKeyPress = self.RESET;
            end
        end
        
        function CheckLightCurtain(self)
            if ArduinoInterface.instance().GetLightCurtainStatus() == ArduinoInterface.instance().STATUS_DETECTED
                self.status = self.STOP;
                return
            end
        end
        
        function status = GetStatus(self)
            self.CheckKeyboard();
            self.CheckLightCurtain();
            status = self.status;
        end
    end
    
    %% Internal method for singleton implementation
    methods(Static)
        function self = instance()
            persistent uniqueInstance
            if isempty(uniqueInstance)
                self = SafetyFeatures();
                uniqueInstance = self;
            else
                self = uniqueInstance;
            end
        end
    end
end

