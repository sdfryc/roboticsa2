//#include <IRremote.h>

#define PIN_DETECT 11

long T_PREV = 0;
long DELTA_T = 0;
int numReadings = 0;

int hasAlreadySent = 0;

void setup()
{
  Serial.begin(9600);
  pinMode(PIN_DETECT, INPUT);
  Serial.println("started");
}

void loop() {
  
  if(digitalRead(PIN_DETECT) == 1)
  {
    // debouncing
    for(int i = 0; i < 10; i++)
    {
      delay(10);
      
      if(digitalRead(PIN_DETECT) == 0)
      {
        break;
      }
      else if(i == 9)
      {
        Serial.println("detected");
        hasAlreadySent = 0;
        while(digitalRead(PIN_DETECT) == 1){
          delay(10);
        }
        break;
      }
    }
  } else {
    if (hasAlreadySent == 0){
      Serial.println("clear");
      hasAlreadySent = 1;
    }
  }
}
